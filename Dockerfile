# Use the official Ubuntu base image
FROM ubuntu:latest

# Update package lists and install necessary dependencies
RUN apt-get update && \
    apt-get install -y python3 python3-pip && \
    apt-get clean

# Install Go
RUN apt-get install -y wget && \
    wget https://golang.org/dl/go1.17.6.linux-amd64.tar.gz && \
    tar -C /usr/local -xzf go1.17.6.linux-amd64.tar.gz && \
    rm go1.17.6.linux-amd64.tar.gz

# Add Go binaries to PATH
ENV PATH=$PATH:/usr/local/go/bin

# Set environment variables for Go
ENV GO111MODULE=on

# Display Go version
RUN go version

# Print Python and Go versions
RUN python3 --version && go version

# Set working directory
#WORKDIR /app

# Copy your application into the container
#COPY . .

# Define the command to run your application
#CMD ["python3", "your_script.py"]
